const {html} = require("@polymer/polymer/lib/utils/html-tag.js");

class RoleForm extends PolymerElement {

    static get template() {
        return html`
  <vaadin-grid items="[[items]]" id="gridRole" style="flex-shrink: 0; width: 100%;"></vaadin-grid>
 </vaadin-vertical-layout>
</vaadin-horizontal-layout>
`;
    }

    static get is() {
        return 'role-form';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(RoleForm.is, RoleForm);
