package masterClass;

import entity.Role;

import java.util.List;

public interface MasterClass {
    List<Role> getAllRoles();
}
