package services;

import entity.Role;
import masterClass.MasterClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService {
    @Autowired
    private MasterClass masterClass;

    public List<Role> getAllRoles() {
        return masterClass.getAllRoles();
    }
}
