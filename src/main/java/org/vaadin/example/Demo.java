package org.vaadin.example;
import java.sql.*;

public class Demo {
    public static void main(String args[]) {
        String url = "jdbc:mysql://localhost:3306/test3";
        String user = "root";
        String password = "mypw";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection(url, user, password);
            System.out.println("Connection is Successful to the database" + url);
            String query = "SELECT * FROM role";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            while(rs.next()) {
                System.out.println(rs.getString("description"));
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}