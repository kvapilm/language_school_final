package controllers;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.EnableVaadin;
import com.vaadin.flow.templatemodel.TemplateModel;
import entity.Role;
import masterClass.MasterClass;
import model.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.vaadin.example.GreetService;
import org.vaadin.example.MainView;
import services.RoleService;

import java.awt.*;
import java.util.List;

@Tag("role")
@JsModule("../../../frontend/src/role-form.js")
public class RoleForm extends PolymerTemplate<RoleForm.RoleFormModel> {
    @Id("gridRole")
    private Grid<Role> gridRole;

    private RoleService roleService;

    public RoleForm(){

    }
    public void init() {

        //grid
        gridRole.addColumn(Role::getId_role).setHeader("ID");
        gridRole.addColumn(Role::getDescription).setHeader("Popis");
        refreshGrid();
    }
    private void refreshGrid() {
        gridRole.setItems((roleService.getAllRoles()));
    }

    public interface RoleFormModel extends TemplateModel {
        // Add setters and getters for template properties here.
    }
}